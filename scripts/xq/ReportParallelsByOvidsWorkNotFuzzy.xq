for $o in db:open("ovidcroala3nf", "ovid3-croala3-notfuzzy.xml")//tr
let $opus := substring-before($o/l[1]/@id, ":")
group by $opus
return ( $opus , count($o))