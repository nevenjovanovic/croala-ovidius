let $clausulae := element versus {
for $croala in collection("croala-cl3")//l
let $id := $croala/@id
let $node := db:node-id($croala)
let $text := substring-after($croala, " ")
order by $text
return if ($text) then element l { attribute node {$node} , $id , $text } else ()
}
return db:create("croala-cl2", $clausulae , "croala-clausulae-2.xml", map { 'ftindex': true(), 'intparse': true(), 'stripns': true() })