declare variable $db1 := "croalatxt";
declare variable $db2 := "ovidcroala3nf";

declare function local:countdocs($db) {
  for $d incollection($db)//*:l/@id
let $id :=  substring-before($d, ":")
group by $id
order by count($d) descending
return if (not(starts-with($id, "phi"))) then 
element tr { element td { $id } , 
element td { count($d) } 
} else ()
};

local:countdocs($db2)