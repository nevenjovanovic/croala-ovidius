(: join Ovid's three word clausulae with matching in CroALa :)
(: use fuzzy search :)
(: 265,000*6700 array :)
(: 100 Ovid's clausulae = 2s search :)
(: 1000 Ovid's clausulae = 24s search :)
(: total time for full search : 157610.85 ms :)
let $body := element tbody {
(: for $n in 1 to 100 :)
let $match := 
for $ovid in collection("ovid-cl3")//l (: [$n] :)
where count(tokenize($ovid, " ")) > 1
let $croala := collection("croala-cl3")//l[text() contains text { $ovid/text() } using fuzzy ]

return if ($croala) then element tr {
  $ovid ,
  $croala
} else ()

return if ($match/*) then $match else ()
}
(: return $body :)
return file:write("/home/neven/Repos/croala-ovidius/results/ovid3-croala3-fuzzy.xml", $body)