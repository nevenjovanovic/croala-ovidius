(: find Ovid's one word clausulae with matches in CroALa, not in two-word matches list :)
(: do not use fuzzy search :)
(: 265,000*6700 array :)
(: total time for full search : 1234.76 ms :)
declare variable $o1 := "ovidcroala1nf";
declare variable $o2 := "ovidcroala2nf";
let $body := element tbody {
(: for $n in 1 to 100 :)
let $match := 
for $ovid in collection($o1)//l[1] (: [$n] :)
let $croala := collection($o2)//l[position()=1 and text() contains text { $ovid/text() } ]

return if ($croala) then ()
 else element tr {
  $ovid/.. }

return if ($match/*) then $match else ()
}
(: return $body :)
return file:write("/home/neven/Repos/croala-ovidius/results/ovid1-croala1-notfuzzy-notovid2.xml", $body)