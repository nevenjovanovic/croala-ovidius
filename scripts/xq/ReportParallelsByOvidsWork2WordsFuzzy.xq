for $o in db:open("ovidcroala2f")//tr
let $opus := substring-before($o/l[1]/@id, ":")
group by $opus
return ( $opus , count($o))