(: create a db of Ovid's clausulae in CroALa from local / corrected copies :)
(: include the Tristia, Ex Ponto :)
let $name := "ovidcroala3nf"
let $body := (
"/home/neven/Repos/croala-ovidius/results/ovid3-croala3-notfuzzy.xml"
)

return db:create($name, $body , (), map { 'ftindex': true(), 'intparse': true(), 'stripns': true() })