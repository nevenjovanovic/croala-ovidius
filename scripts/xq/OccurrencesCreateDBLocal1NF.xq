(: create a db of Ovid's clausulae in CroALa from local / corrected copies :)
(: include the Tristia, Ex Ponto :)
let $name := "ovidcroala1nf"
let $body := (
"/home/neven/Repos/croala-ovidius/results/ovid1long-croala1long-notfuzzy.xml"
)

return db:create($name, $body , (), map { 'ftindex': true(), 'intparse': true(), 'stripns': true() })