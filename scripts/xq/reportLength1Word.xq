for $l in collection("ovidcroala1nf")//*:l
let $len := string-length($l)
group by $len
order by $len descending
return ( $len , count($l))