(: join Ovid's single word clausulae with matching in CroALa single word DB :)
(: do not use fuzzy search :)
(: 265,000*6700 array :)
(: total time for full search : 1234.76 ms :)
let $body := element tbody {
(: for $n in 1 to 100 :)
let $match := 
for $ovid in collection("ovid-cl1-long")//l (: [$n] :)
let $croala := collection("croala-cl1-long")//l[text() contains text { $ovid/text() } ]

return if ($croala) then element tr {
  $ovid ,
  $croala
} else ()

return if ($match/*) then $match else ()
}
(: return $body :)
return file:write("/home/neven/Repos/croala-ovidius/results/ovid1long-croala1long-notfuzzy.xml", $body)