(: create a db of Ovid's clausulae in CroALa from local / corrected copies :)
(: include the Tristia, Ex Ponto :)
let $name := "ovidcroala1nfnot2nf"
let $body := (
"/home/neven/Repos/croala-ovidius/results/ovid1-croala1-notfuzzy-notovid2.xml"
)

return db:create($name, $body , (), map { 'ftindex': true(), 'intparse': true(), 'stripns': true() })