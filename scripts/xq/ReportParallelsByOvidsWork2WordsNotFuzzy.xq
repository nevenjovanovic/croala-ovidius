for $o in db:open("ovidcroala2nf")//tr
let $opus := substring-before($o/l[1]/@id, ":")
group by $opus
return ( $opus , count($o))