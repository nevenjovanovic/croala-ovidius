declare function local:escape-for-regex
  ( $arg ) {

   replace($arg,
           '(\.|\[|\]|\\|\||\-|\^|\$|\?|\*|\+|\{|\}|\(|\))','\\$1')
 } ;

declare function local:substring-after-last
  ( $arg ,
    $delim )  {

   replace ($arg,concat('^.*',local:escape-for-regex($delim)),'')
 } ;

let $clausulae := element versus {
for $ovid in collection("ovid-cl3")//l
let $id := $ovid/@id
let $node := db:node-id($ovid)
let $text := local:substring-after-last($ovid, " ")
where string-length($text) >= 7
order by $text
return element l { attribute node {$node} , $id , $text }
}

return db:create("ovid-cl1-long", $clausulae , "ovid-clausulae-1-longae.xml", map { 'ftindex': true(), 'intparse': true(), 'stripns': true() })