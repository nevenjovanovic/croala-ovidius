declare variable $db1 := "croalatxt";
declare variable $db2 := "croala-cl3";
declare variable $db3 := "ovidcroala3nf";

let $docverses := element tbody {
for $d in collection($db2)//*:l/@id
let $doc := substring-before($d, ":")
group by $doc
return element tr {
  element td { $doc } , 
  element td { count($d) }
}
}

for $d in $docverses/*
let $date := db:open($db1, $d/td[1])//*:teiHeader/*:profileDesc[1]/*:creation/*:date[1]/@period
let $ovid := count(db:open($db3)//*:l[starts-with(@id, $d/td[1])])
group by $date
order by $date
return ( $date || "," || count($d) || "," || sum($d/td[2]) || "," || sum($ovid) )
