let $clausulae := element versus {
for $ovid in collection("ovid-cl3")//l
let $id := $ovid/@id
let $node := db:node-id($ovid)
let $text := substring-after($ovid, " ")
order by $text
return element l { attribute node {$node} , $id , $text }
}
return db:create("ovid-cl2", $clausulae , "ovid-clausulae-2.xml", map { 'ftindex': true(), 'intparse': true(), 'stripns': true() })