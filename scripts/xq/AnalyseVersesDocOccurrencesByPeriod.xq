declare variable $db1 := "croalatxt";
declare variable $db2 := "ovidcroala3nf";

let $doclist := 
distinct-values(
  for $d incollection($db2)//*:l/@id
  return  substring-before($d, ":")
)
for $d in $doclist
let $date := db:open($db1, $d)//*:teiHeader/*:profileDesc[1]/*:creation/*:date[1]/@period
group by $date
order by $date
return if ($date) then element d { $date , count($d) } else ()