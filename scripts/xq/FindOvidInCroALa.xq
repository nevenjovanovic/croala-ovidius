declare variable $db1 := "ovid-cl3";
declare variable $db2 := "croala-cl3";
declare variable $test := "a funere differt";
for $n in 1 to 10
let $c1 := collection($db1)//*:l[$n]
where $test[. contains text { $c1 } using fuzzy ]
return $c1