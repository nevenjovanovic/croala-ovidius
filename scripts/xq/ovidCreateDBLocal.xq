(: create a db of CroALa files from local / corrected copies :)
(: include the Tristia, Ex Ponto, Ibis :)

let $body := (
"/home/neven/Repos/croala-ovidius/data/ovidius/"
)

return db:create("ovid-pdl2", $body , (), map { 'ftindex': true(), 'intparse': true(), 'stripns': true() })