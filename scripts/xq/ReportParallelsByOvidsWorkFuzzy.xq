for $o in db:open("ovidcroala3f", "ovid3-croala3-fuzzy.xml")//tr
let $opus := substring-before($o/l[1]/@id, ":")
group by $opus
return ( $opus , count($o))