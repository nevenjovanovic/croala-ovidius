declare function local:normspell($text){
  let $clear := replace(normalize-space(string-join($text, " ")), "[\p{P}\p{C}]+", " ")
let $low := lower-case($clear)
let $uv := replace($low, "v", "u")
let $ij := replace($uv, "[yj]", "i")
let $num := replace($ij, "[0-9]+", "")
let $ae := replace(
  replace($num, "ę", "e"), 
  "ae", "e")
let $accents := replace(normalize-unicode($ae,"NFD"),"\p{IsCombiningDiacriticalMarks}","")
return $accents
};

declare function local:claus3($acc2){
  ( 
ft:tokenize($acc2)[last() - 2] , 
ft:tokenize($acc2)[last() - 1] , 
ft:tokenize($acc2)[last()] 
)
};
let $versus := element versus {
let $clausulae :=
for $l in collection("ovid-pdl2")//*:text//*:l
let $id := db:path($l) || ":" || db:node-id($l)
let $text := $l//text()[not(ancestor::*:note)]
let $acc2 := local:normspell($text)
let $cl3 := local:claus3($acc2)
return element l { 
attribute id {$id} ,
$cl3
}
for $c in $clausulae
order by $c
return if ($c/text()) then $c else()
}
return db:create("ovid-cl3", $versus , "ovid-clausulae-3.xml", map { 'ftindex': true(), 'intparse': true(), 'stripns': true() })