let $clausulae :=
for $l in collection("croalatxt")//*:text//*:l
let $id := db:path($l) || ":" || db:node-id($l)
let $text := $l//text()[not(parent::*:note)]
let $clear := replace(normalize-space(string-join($text, " ")), "[\p{P}\p{C}]+", "")
let $low := lower-case($clear)
let $uv := replace($low, "v", "u")
let $ij := replace($uv, "[yj]", "i")
let $num := replace($ij, "[0-9]+", "")
let $ae := replace(replace($num, "ę", "e"), "ae", "e")
let $accents := replace(normalize-unicode($ae,"NFD"),"\p{IsCombiningDiacriticalMarks}","")
let $cl3 := ( ft:tokenize($accents)[last() - 2] , ft:tokenize($accents)[last() - 1] , ft:tokenize($accents)[last()] )
return element l { 
attribute id {$id} ,
$cl3
}
for $c in $clausulae
order by $c
return if ($c/text()) then $c else()