declare variable $db := "croalatxt";
distinct-values(
for $l in collection($db)//*:text//*:l
let $id := db:path($l) || ":" || db:node-id($l)
let $text := $l//*/name()
return $text
)