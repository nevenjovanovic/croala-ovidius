declare function local:escape-for-regex
  ( $arg ) {

   replace($arg,
           '(\.|\[|\]|\\|\||\-|\^|\$|\?|\*|\+|\{|\}|\(|\))','\\$1')
 } ;

declare function local:substring-after-last
  ( $arg ,
    $delim )  {

   replace ($arg,concat('^.*',local:escape-for-regex($delim)),'')
 } ;

let $clausulae := element versus {
for $croala in collection("croala-cl3")//l
let $id := $croala/@id
let $node := db:node-id($croala)
let $text := local:substring-after-last($croala, " ")
where string-length($text) >= 7
order by $text
return element l { attribute node {$node} , $id , $text }
}

return db:create("croala-cl1-long", $clausulae , "croala-clausulae-1-longae.xml", map { 'ftindex': true(), 'intparse': true(), 'stripns': true() })