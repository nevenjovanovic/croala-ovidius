declare variable $db1 := "ovid-cl3";
declare variable $db2 := "croala-cl3";
declare variable $test := "a funere differt";

for $c1 in collection($db2)//*:l
let $id := substring-before($c1/@id, ":")
group by $id
order by count($c1) descending
return element doc { attribute id {$id} , count($c1) }