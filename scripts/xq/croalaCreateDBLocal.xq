(: create a db of CroALa files from local / corrected copies :)

let $body := (
"/home/neven/Repos/croalatxt/txts/"
)

return db:create("croalatxt", $body , (), map { 'ftindex': true(), 'intparse': true(), 'stripns': true() })