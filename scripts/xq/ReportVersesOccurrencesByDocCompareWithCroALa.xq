declare variable $db1 := "croalatxt";
declare variable $db2 := "ovidcroala3nf";
declare variable $db3 := "croala-cl3";

declare function local:countdocs($db) {
  element tbody {
  for $d incollection($db)//*:l/@id
let $id :=  substring-before($d, ":")
group by $id
order by count($d) descending
return if (not(starts-with($id, "phi"))) then 
element tr { element td { $id } , 
element td { count($d) } 
} else ()
}
};

let $croala := local:countdocs($db3)
let $croalaovid := local:countdocs($db2)
for $t in $croala/tr
let $ovid := $croalaovid/tr[td[1]=$t/td[1]]/td[2]
return element tr {
  $t/td[1],
  $t/td[2],
  if ($ovid) then $ovid else element td { "0"},
  if ($ovid) then element td {
    xs:integer($ovid) div xs:integer($t/td[2]) * 100 } else element td { "0"}
}