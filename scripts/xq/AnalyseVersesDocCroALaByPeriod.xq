declare variable $db1 := "croalatxt";
declare variable $db2 := "croala-cl3";

let $doclist := 
distinct-values(
  for $d in collection($db2)//*:l/@id
  return  substring-before($d, ":")
)
for $d in $doclist
let $date := db:open($db1, $d)//*:teiHeader/*:profileDesc[1]/*:creation/*:date[1]/@period
let $verses := count(collection($db2)//*:l[starts-with(@id, $d)])
group by $date
order by $date
return ( $date || "," || count($d) || "," || $verses )