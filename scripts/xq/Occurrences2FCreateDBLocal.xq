(: create a db of Ovid's 2-word clausulae in CroALa, found using fuzzy search :)
let $name := "ovidcroala2f"
let $body := (
"/home/neven/Repos/croala-ovidius/results/ovid2-croala2-fuzzy.xml"
)

return db:create($name, $body , (), map { 'ftindex': true(), 'intparse': true(), 'stripns': true() })